// TODO: need to be replaced with a native implementation
function padStart (string, length, chars) {
  string = String(string)
  while (string.length < length) {
    string = chars + string
  }
  return string
}

export function secondsToTime (secs) {
  let minutes = Math.floor(secs / 60)
  let seconds = Math.ceil(secs - (minutes * 60))
  minutes = padStart(minutes, 2, '0')
  seconds = padStart(seconds, 2, '0')
  return [minutes, seconds]
}

export function timeRange (min, max, step) {
  min = Number(min)
  max = Number(max)
  step = Number(step)
  if (!(min && max && step)) throw new Error('timeRange took NaN')
  const arr = []
  for (let i = min / step; i < (max / step) + 1; i++) {
    const time = secondsToTime(i * step)
    arr.push({
      view: time[0] + ':' + time[1],
      seconds: i * step
    })
  }
  return arr
}

export function makeDataSnapshot (...arg) {
  return arg.map(el => JSON.stringify(el))
}

export function hasDataSnapshotChanged (dataSnapshot, ...arg) {
  if (dataSnapshot.length !== arg.length) throw new Error("DataSnapshots' length should be equal")
  if (arg.length === 1) return dataSnapshot[0] !== JSON.stringify(arg[0])
  return arg.map((el, i) => {
    return dataSnapshot[i] !== JSON.stringify(el)
  })
}

export function difference (array1, array2) {
  if (array1.length > array2.length) {
    return array1.filter(i => array2.indexOf(i) < 0)
  } else {
    return array2.filter(i => array1.indexOf(i) < 0)
  }
}

export function deepClone (obj) {
  const clObj = Array.isArray(obj) ? [] : {}
  for (const i in obj) {
    if (obj[i] instanceof Object) {
      clObj[i] = deepClone(obj[i])
      continue
    }
    clObj[i] = obj[i]
  }
  return clObj
}

// Not deep copying. Use deepClone function before if you need.
export function collectSomeProperties (obj, properties, additionObj) {
  let newObj = {}
  properties.forEach(p => {
    newObj[p] = obj[p]
  })
  if (additionObj) {
    newObj = Object.assign(newObj, additionObj)
  }
  return newObj
}

export function localStorageParse (item) {
  try {
    return JSON.parse(localStorage.getItem(item))
  } catch (e) {
    console.error('localStorageParse error:', e)
    return null
  }
}

export function relocateToAnotherArray (givingArr, receivingArr, callback) {
  // перемещает значение первого найденного в массиве элемента в конец другого массива
  const index = givingArr.findIndex(callback)
  if (index === -1) return false
  receivingArr.push(givingArr[index])
  givingArr.splice(index, 1)
}

export function getObjValueByPath (obj, path) {
  const dotIdx = path.indexOf('.')
  if (dotIdx === 0) return undefined
  if (dotIdx === -1) return obj[path]
  return getObjValueByPath(obj[path.substring(0, dotIdx)], path.substring(dotIdx + 1))
}
