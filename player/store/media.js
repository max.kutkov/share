import { set } from '@/utils/vuex'
import { getMedia } from '@/utils/media'
import handlingErrors from '@/error/handling'
import { localStorageParse } from '@/utils/helper'

const state = {
  file: null, // blob file or url
  id: null, // media id
  status: null, // playing, paused, finished
  snackbar: false,
  list: new Map(localStorageParse('mediaCache')) || new Map()
}

const actions = {
  SetMedia ({ state, commit, dispatch }, payload) {
    if (payload.file && typeof payload.file === 'object') { // local file
      commit('setMedia', payload)
    } else {
      const expiredMedia = []
      state.list.forEach(val => {
        if (val.expire <= Date.now()) expiredMedia.push(val) // delete expired media
      })
      commit('deleteFromList', expiredMedia)
      const cacheFile = state.list.get(payload.id) // id = type + composite id inside type
      if (cacheFile) {
        commit('setMedia', cacheFile)
      } else {
        getMedia(payload.id)
          .then(r => {
            commit('setMedia', {
              id: payload.id,
              status: payload.status,
              file: r.data.url,
              expire: Date.now() + r.data.url_expire * 1000
            })
          })
          .catch(e => handlingErrors(e))
      }
    }
  }
}

const mutations = {
  setMedia (state, payload) {
    state.file = payload.file
    state.id = payload.id
    state.status = 'paused'
    if (!state.snackbar) state.snackbar = true
    setTimeout(() => {
      state.status = payload.status
    }, 0)
    if (typeof payload.file !== 'object') {
      state.list.set(payload.id, payload)
      localStorage.setItem('mediaCache', JSON.stringify(Array.from(state.list)))
    }
  },
  deleteFromList (state, arr) {
    arr.forEach(el => {
      state.list.delete(el.id)
    })
    localStorage.setItem('mediaCache', JSON.stringify(Array.from(state.list)))
  },
  closePlayer (state) {
    state.snackbar = false
    state.status = 'paused'
  },
  setStatus (state, payload) {
    if (!state.snackbar) state.snackbar = true
    state.status = payload
  },
  setSnackbar: set('snackbar')
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
